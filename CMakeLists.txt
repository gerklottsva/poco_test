cmake_minimum_required(VERSION 3.5)
project(poco_test LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_EXE_LINKER_FLAGS "-no-pie")

find_package(Poco REQUIRED Net)
add_executable(poco_test main.cpp)
target_link_libraries(poco_test Poco::Net)
