#include <iostream>
#include <Poco/Net/TCPServer.h>

using Poco::Net::StreamSocket;
using Poco::Net::TCPServer;
using Poco::Net::SocketAddress;
using Poco::Net::TCPServerConnectionFactoryImpl;

class EchoConnection : public Poco::Net::TCPServerConnection
{
public:
    EchoConnection(const StreamSocket& s) : TCPServerConnection(s)
    {

    }

    void run() override
    {
        StreamSocket& ss = socket();

        char* str {"Welcome to POCO TCP server. Enter you string:\n"};
        ss.sendBytes(str, strlen(str));

        try
        {
            char buffer[255] = "";
            int n = ss.receiveBytes(buffer, sizeof(buffer));

            while (n > 0)
            {
                std::reverse(buffer, buffer + strlen(buffer) - 2);

                ss.sendBytes(buffer, n);
                memset(buffer, 0, 255);

                n = ss.receiveBytes(buffer, sizeof(buffer));
            }
        }
        catch (Poco::Exception& exc)
        {
            std::cerr << "EchoConnection: " << exc.displayText() << std::endl;
        }
    }
};

int main(int argc, char** argv)
{
    TCPServer srv(new TCPServerConnectionFactoryImpl<EchoConnection>(), 28888);

    srv.start();

    std::string str;

    while(true)
    {
        str.clear();

        std::cout << "To stop server type \"exit\":" << std::endl;
        std::cin >> str;

        if(str == "exit")
        {
            break;
        }
    }

    srv.stop();

    return 0;
}
